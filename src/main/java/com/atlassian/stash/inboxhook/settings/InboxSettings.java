package com.atlassian.stash.inboxhook.settings;

import com.atlassian.sal.api.pluginsettings.PluginSettings;

/**
 * Persists and retrieves inbox hook settings.
 */
public class InboxSettings {

    private static final String READY_TO_MERGE = "readyToMerge";
    private static final String REQUIRE_APPROVAL = "requireApproval";

    private final PluginSettings pluginSettings;
    private final String userKey;

    public InboxSettings(PluginSettings pluginSettings, String username) {
        this.pluginSettings = pluginSettings;
        this.userKey = username;
    }

    public boolean isShowReadyToMerge() {
        return get(READY_TO_MERGE, true);
    }

    public boolean isShowRequireApproval() {
        return get(REQUIRE_APPROVAL, true);
    }

    private boolean get(String key, boolean defaultValue) {
        String value = (String) pluginSettings.get(namespace(userKey, key));
        return value == null ? defaultValue : Boolean.parseBoolean(value);
    }

    public void setShowReadyToMerge(boolean value) {
        set(READY_TO_MERGE, value);
    }

    public void setShowRequireApproval(boolean value) {
        set(REQUIRE_APPROVAL, value);
    }

    private void set(String key, boolean value) {
        pluginSettings.put(namespace(userKey, key), Boolean.toString(value));
    }

    private static String namespace(String userKey, String key) {
        return String.format("inbox.hook.settings.%s.%s", userKey, key);
    }

}
